export default {
    agent_campaign: {
        name: 'نام',
        username: 'Username',
        sip: 'ID SIP',
        penalty: 'هزینه جریمه'
    },
    pause_set: {
        id: 'شناسه',
        name: 'نام'
    },
    pause_setting: {
        id: 'شناسه',
        pause: 'مکث',
        pause_type: 'نوع مکث',
        set: 'تنظیم',
        time_to_end_pause: 'زمان پایان دادن به مکث'
    },
    audit: {
        user: 'کاربر',
        object: 'هدف - شی',
        name: 'نام',
        action: 'عمل',
        additional_information: 'تغییر دادن',
        datetime: 'تاریخ و زمان'
    },
    external_site: {
        id: 'برو',
        name: 'نام',
        url: 'URL',
        method: 'روش',
        format: 'قالب',
        objective: 'هدف',
        trigger: 'ماشه',
        status: 'شرایط. شرط'
    },
    call_disposition: {
        id: 'شناسه',
        name: 'نام'
    }
};
