export default {
    agent_campaign: {
        name: 'Nome',
        username: 'Username',
        sip: 'ID SIP',
        penalty: 'Multa'
    },
    pause_set: {
        id: 'ID',
        name: 'Nome'
    },
    pause_setting: {
        id: 'ID',
        pause: 'Pausa',
        pause_type: 'Tipo de pausa',
        set: 'Conjunto',
        time_to_end_pause: 'Tiempo para terminar pausa'
    },
    audit: {
        user: 'Usuario',
        object: 'Objeto',
        name: 'Nome',
        action: 'Açao',
        additional_information: 'Mudar',
        datetime: 'Data e hora'
    },
    external_site: {
        id: 'ID',
        name: 'Nome',
        url: 'URL',
        method: 'Método',
        format: 'Formato',
        objective: 'Meta',
        trigger: 'Acionar',
        status: 'Doença'
    },
    call_disposition: {
        id: 'ID',
        name: 'Nome'
    }
};
