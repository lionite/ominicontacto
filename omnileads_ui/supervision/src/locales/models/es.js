export default {
    agent_campaign: {
        name: 'Nombre',
        username: 'Nombre de usuario',
        sip: 'ID SIP',
        penalty: 'Multa'
    },
    pause_set: {
        id: 'ID',
        name: 'Nombre'
    },
    pause_setting: {
        id: 'ID',
        pause: 'Pausa',
        pause_type: 'Tipo de pausa',
        set: 'Conjunto',
        time_to_end_pause: 'Tiempo para terminar pausa'
    },
    audit: {
        user: 'Usuario',
        object: 'Objeto',
        name: 'Nombre',
        action: 'Accion',
        additional_information: 'Información Adicional',
        datetime: 'Fecha y hora'
    },
    external_site: {
        id: 'ID',
        name: 'Nombre',
        url: 'Url',
        method: 'Metodo',
        format: 'Formato',
        objective: 'Objetivo',
        trigger: 'Disparador',
        status: 'Estado'
    },
    call_disposition: {
        id: 'ID',
        name: 'Nombre'
    }
};
