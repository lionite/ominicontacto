export default {
    agent_campaign: {
        name: 'Name',
        username: 'Username',
        sip: 'ID SIP',
        penalty: 'Penalty'
    },
    pause_set: {
        id: 'ID',
        name: 'Name'
    },
    pause_setting: {
        id: 'ID',
        pause: 'Pause',
        pause_type: 'Pause type',
        set: 'Set',
        time_to_end_pause: 'Time to end pause'
    },
    audit: {
        user: 'User',
        object: 'Object',
        name: 'Name',
        action: 'Action',
        additional_information: 'Additional Information',
        datetime: 'Datetime'
    },
    external_site: {
        id: 'ID',
        name: 'Name',
        url: 'Url',
        method: 'Method',
        format: 'Format',
        objective: 'Objective',
        trigger: 'Trigger',
        status: 'Status'
    },
    call_disposition: {
        id: 'ID',
        name: 'Name'
    }
};
